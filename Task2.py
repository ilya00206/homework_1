#!/usr/bin/env python3
"""Task from page 29 """


class Dog:
    pass


class LipStick:
    pass


class Cake:
    pass


retriever: Dog = Dog()
print(retriever)

red_lipstick: LipStick = LipStick()
print(red_lipstick)

cheesecake: Cake = Cake()
print(cheesecake)
